This addon enables replying to JIRA comments and comment threading.
This works without modifying the comment data storage in JIRA which enables to restore the JIRA functionality by just uninstalling the plugin.
Note the following:
- With Jira 7.2.0 the Atlassian Wikieditor will be used.
- For the full set of feature (espacially previewing comments) is Jira 7.2.0 required
- Thirdparty Wikieditor like JEditor are not supported
- If autocompletion for @mention isn't working see: https://confluence.atlassian.com/jirakb/mentions-and-autocomplete-are-not-working-in-jira-cloud-779160727.html

Please be patient for the addon updates for major JIRA releases.
Please avoid writing bad reviews instead of raising a bug report
